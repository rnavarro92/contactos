
        $( "#crearForm" ).submit(function( event ) {
          event.preventDefault();
            $.post('crear-usuarios.php',
              $('#crearForm').serialize(),
              function(data) {
                if (data.exito != true){
                    //alert('Contacto RUN: ' + data.rut + ' ya existe');
                    $('#mensaje').html('Contacto RUN: ' + data.rut + ' ya existe');
                    $('#modalMensaje').modal('open');                
                }else{
                    $('#tablaContactos tr:last').after(
                         '<tr id="tr_'+data.id+'">'+
                         '<td id="rut_'+data.id+'">'+data.rut+'</td>'+
                         '<td id="nombre_'+data.id+'">'+data.nombre+'</td>'+
                         '<td id="email_'+data.id+'">'+data.mail+'</td>'+
                         '<td id="telefono_'+data.id+'">'+data.telefono+'</td>'+
                         '<td id="cargo_'+data.id+'">'+data.cargo+'</td>'+
                         '<td id="accion_'+data.id+'"><a class="waves-effect waves-teal btn-flat" data-toggle="modal" href="#updateM_'+data.id+'">Actualizar</a> <a class="waves-effect waves-teal btn-flat" data-toggle="modal" href="#deleteM_'+data.id+'">Eliminar</a></td>'+
                         '</tr>'
                         );
                    $("#crearModal").modal('close');
                    location.reload();//Actualizo la pagina ya que no pude resolver un problema, el cual consiste en que sino
                    //recargo la pagina, al momento de eliminar, elimina el registro de la tabla(pantalla) pero no de la base 
                    //de datos entonces al volver a recargar la pagina vuelve a aparecer el registro en la tabla(pantalla).
                     }    
            });
        });