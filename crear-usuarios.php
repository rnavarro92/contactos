<?php
require('conexion.php');
$rut = $_POST['in-rut'];
$nombre = $_POST['in-nombre'];
$mail = $_POST['in-mail'];
$telefono = $_POST['in-telefono'];
$cargo= $_POST['cargo'];

$filter = ['run' => $rut];

$query = new MongoDB\Driver\Query($filter);     
    
$res = $mng->executeQuery("almacen.contactos", $query);
    
    $busqueda = current($res->toArray());
    
    if (!empty($busqueda)) {
    
	    header('Content-Type: application/json');
	    echo json_encode(array('exito'=>false, 'rut'=>$rut));
	            
    } else {
    	        
		$valor = new MongoDB\BSON\ObjectID();
		$contacto = ['_id' => $valor, 'run' => $rut, 'nombre' => $nombre, 'email' => $mail, 'telefono' => $telefono, 'cargo' => $cargo];

		$bulk->insert($contacto);

		$mng->executeBulkWrite('almacen.contactos', $bulk);

		$id = (string) $valor;

	    header('Content-Type: application/json');
	    echo json_encode(array('exito'=>true, 'id' => $id, 'rut'=>$rut, 'nombre'=>$nombre, 'mail'=>$mail, 'telefono'=> $telefono, 'cargo'=>$cargo));
    }
?>