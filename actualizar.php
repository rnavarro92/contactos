<?php
require('conexion.php');
$rut = $_POST['in-rut'];
$nombre = $_POST['in-nombre'];
$mail = $_POST['in-mail'];
$telefono = $_POST['in-telefono'];
$cargo= $_POST['cargo'];
$id=$_POST['update_id'];
$urut=$_POST['update_rut'];

if ($rut != $urut){
	$filter = ['run' => $rut];
	$query = new MongoDB\Driver\Query($filter);     
	$res = $mng->executeQuery("almacen.contactos", $query);
	$busqueda = current($res->toArray());
    if (!empty($busqueda)) {
	    header('Content-Type: application/json');
	    echo json_encode(array('exito'=>false, 'rut'=>$rut));   
    }else{

    	$contacto = ['run' => $rut, 'nombre' => $nombre, 'email' => $mail, 'telefono' => $telefono, 'cargo' => $cargo];
		$bulk->update(['_id' => new MongoDB\BSON\ObjectID("$id")], ['run' => $rut, 'nombre' => $nombre, 'email' => $mail, 'telefono' => $telefono, 'cargo' => $cargo]);
		$mng->executeBulkWrite('almacen.contactos', $bulk);
		    header('Content-Type: application/json');
		    echo json_encode(array('exito'=>true, 'id'=>$id,'rut'=>$rut, 'nombre'=>$nombre, 'mail'=>$mail, 'telefono'=> $telefono, 'cargo'=>$cargo));

    }
}else{
	$contacto = ['run' => $rut, 'nombre' => $nombre, 'email' => $mail, 'telefono' => $telefono, 'cargo' => $cargo];
		$bulk->update(['_id' => new MongoDB\BSON\ObjectID("$id")], ['run' => $rut, 'nombre' => $nombre, 'email' => $mail, 'telefono' => $telefono, 'cargo' => $cargo]);
		$mng->executeBulkWrite('almacen.contactos', $bulk);
		    header('Content-Type: application/json');
		    echo json_encode(array('exito'=>true, 'id'=>$id,'rut'=>$rut, 'nombre'=>$nombre, 'mail'=>$mail, 'telefono'=> $telefono, 'cargo'=>$cargo));
}
?>