<table class="table bordered highlight" id="tablaContactos">
                    <thead>
                        <tr>
                          <th>RUN</th>
                          <th>Nombre</th>
                          <th>Email</th>
                          <th>Teléfono</th>
                          <th>Cargo</th>
                          <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                        require('./conexion.php');
                        $rows = $mng->executeQuery("almacen.contactos", $query);
                        foreach ($rows as $row) {
                          echo "<tr id=tr_$row->_id>";
                          echo "<td id=rut_$row->_id>$row->run</td>";
                          echo "<td id=nombre_$row->_id>$row->nombre</td>";
                          echo "<td id=email_$row->_id>$row->email</td>";
                          echo "<td id=telefono_$row->_id>$row->telefono</td>";
                          echo "<td id=cargo_$row->_id>$row->cargo</td>";
                          echo "<td id=accion_$row->_id><a class='waves-effect waves-teal btn-flat' data-toggle='modal' href=#updateM_$row->_id>Actualizar</a>";
						  echo "<a class='waves-effect waves-teal btn-flat' data-toggle='modal' href=#deleteM_$row->_id>Eliminar</a></td>";
                          echo "</tr>";
                        }
                      ?>
                    </tbody>
</table>
