
              <div class="modal" id="crearModal"\>
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title">Crear Contacto</h4>
                        </div>
                        <div class="modal-body">
                        <form id="crearForm" class="form">
                          <div class="input-field">
                            <input type="text" name="in-rut" class="inputRut active" required="required" maxlength="12">
                            <label for="in-rut" class="active">
                              Ingrese RUT:
                            </label>
                          </div>
                          <div class="input-field">
                            <input type="text" name="in-nombre" id="in-nombre" class="active" required="required">
                            <label for="in-nombre" class="active">
                              Ingrese Nombre:
                            </label>
                          </div>
                      <div class="input-field">
                      <label class="active">Cargo:</label>
                        <select name="cargo" class="form-control" required="required"> 
                          <option value="" disabled selected>Ingrese un cargo</option>
                          <option value="Gerente">Gerente</option>
                          <option value="Supervisor">Supervisor</option>
                          <option value="Administrador">Administrador</option>
                          <option value="Soporte">Soporte</option>
                          <option value="Abogado">Abogado</option>
                          <option value="Analista">Analista</option>
                          <option value="Coordinador">Coordinador</option>
                          <option value="Disenador">Disenador</option>
                          <option value="Tecnico">Tecnico</option>
                        </select>
                      </div>
                          <div class="input-field">
                            <input type="text" name="in-mail" id="in-mail" class="active" required="required">
                            <label for="in-mail" class="active">
                              Ingrese E-Mail:
                            </label>
                          </div>
                          <div class="input-field">
                            <label for="in-telefono" class="active">
                              Ingrese Teléfono:
                            </label>
                            <input type="text" name="in-telefono" id="in-telefono" class="active" required="required" maxlength="9" onkeypress="return validarTelefono(event)">
                          </div>
                        </div>
                        <div class="modal-footer">
                          <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancelar</a>
                          <input type="submit" class="botonContacto btn btn-primary" value="Crear">
                        </div>
                        </form> 
                      </div>
                    </div>
                  </div>
