<?php 

             $rowD = $mng->executeQuery("almacen.contactos", $query);
          foreach ($rowD as $row){
                echo "<div class=\"modal\" id='updateM_$row->_id'>
                    <div class=\"modal-dialog\">
                      <div class=\"modal-content\">
                        <div class=\"modal-header\">
                          <h4 class=\"modal-title\">Editar Contacto</h4>
                        </div>
                        <div class=\"modal-body\">
                        <form role=\"form\" method=\"POST\" id='update_$row->_id'>
                        <br>
                          <input type=\"hidden\" name=\"update_id\" value='$row->_id'>
                          <input type=\"hidden\" name=\"update_rut\" value='$row->run'>
                          <div class=\"input-field\">
                            <label for=\"in-rut\" class=\"active\">
                              RUT:
                            </label>
                            <input type=\"text\" name=\"in-rut\" class=\"inputRut active\" value='$row->run' maxlength=\"12\">
                          </div>
                          <div class=\"input-field\">
                            <label for=\"in-nombre\" class=\"active\">
                              Nombre:
                            </label>
                            <input type=\"text\" name=\"in-nombre\" class=\"active\" value='$row->nombre'>
                          </div>
                          ";
                          ?>
                          <div class="input-field">
                      <label class="active">Cargo:</label>
                        <select name="cargo" class="form-control" required="required"> 
                          <option value="" disabled selected>Ingrese un cargo</option>
                          <option value="Gerente" <?php if ($row->cargo == 'Gerente') echo 'selected="selected";' ?> >Gerente</option>
                          <option value="Supervisor" <?php if ($row->cargo == 'Supervisor') echo 'selected="selected";' ?>>Supervisor</option>
                          <option value="Administrador" <?php if ($row->cargo == 'Administrador') echo 'selected="selected";' ?>>Administrador</option>
                          <option value="Soporte" <?php if ($row->cargo == 'Soporte') echo 'selected="selected";' ?>>Soporte</option>
                          <option value="Abogado" <?php if ($row->cargo == 'Abogado') echo 'selected="selected";' ?>>Abogado</option>
                          <option value="Analista" <?php if ($row->cargo == 'Analista') echo 'selected="selected";' ?>>Analista</option>
                          <option value="Coordinador" <?php if ($row->cargo == 'Coordinador') echo 'selected="selected";' ?>>Coordinador</option>
                          <option value="Disenador" <?php if ($row->cargo == 'Disenador') echo 'selected="selected";' ?>>Disenador</option>
                          <option value="Tecnico" <?php if ($row->cargo == 'Tecnico') echo 'selected="selected";' ?>>Tecnico</option>
                        </select>
                      </div>
                          <?php
               
                echo "        <div class=\"input-field\">
                            <label for=\"in-mail\" class=\"active\">
                              E-mail:
                            </label>
                            <input type=\"text\" name=\"in-mail\" class=\"active\" value='$row->email'>
                          </div>
                          <div class=\"input-field\">
                            <label for=\"in-telefono\" class=\"active\">
                              Teléfono:
                            </label>
                            <input type=\"text\" name=\"in-telefono\" class=\"in-telefono active\" value='$row->telefono' maxlength=\"9\" onkeypress=\"return validarTelefono(event)\">
                          </div>
                        </div>
                        <div class=\"modal-footer\">
                          <a href=\"#!\" class=\"modal-action modal-close waves-effect waves-green btn-flat\">Cancelar</a>
                          <input type=\"submit\" class=\"botonContacto btn btn-primary\" value=\"Actualizar\" onclick=actualizar('$row->_id') >
                        </div>
                        </form> 
                      </div>
                    </div>
                  </div>";
                }
                ?>