/* 1 */
{
    "_id" : ObjectId("582a08f4f3c52abcad69bf40"),
    "run" : "18.148.147-1",
    "nombre" : "Ricardo Navarro",
    "email" : "r.navarro03@ufromail.cl",
    "telefono" : "989176167",
    "cargo" : "Analista"
}

/* 2 */
{
    "_id" : ObjectId("582a208df3c52abcad69bf43"),
    "run" : "18.789.323-1",
    "nombre" : "Homero Simpson",
    "email" : "homero@gmail.us",
    "telefono" : "98923371",
    "cargo" : "Abogado"
}

/* 3 */
{
    "_id" : ObjectId("582a2231f3c52abcad69bf44"),
    "run" : "17.832.365-6",
    "nombre" : "Juana Nieves",
    "email" : "j.nieves@gmail.com",
    "telefono" : "987456321",
    "cargo" : "Secretaria"
}

/* 4 */
{
    "_id" : ObjectId("582a2268f3c52abcad69bf45"),
    "run" : "16.234.543-6",
    "nombre" : "Rick Harrison",
    "email" : "rick@gmail.com",
    "telefono" : "932145678",
    "cargo" : "Tecnico"
}

/* 5 */
{
    "_id" : ObjectId("582a22a6f3c52abcad69bf46"),
    "run" : "12.658.145-3",
    "nombre" : "Marta Sanchez",
    "email" : "marta.sanchez@gmail.com",
    "telefono" : "965421378",
    "cargo" : "Enfermera"
}

/* 6 */
{
    "_id" : ObjectId("582a22fdf3c52abcad69bf47"),
    "run" : "06.419.854-k",
    "nombre" : "Alexi Laiho",
    "email" : "aleksi.laihok@gmail.fn",
    "telefono" : "846521356",
    "cargo" : "Soporte"
}