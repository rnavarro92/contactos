<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Contactos</title>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="css/estilo.css"/>
</head>
<body>
    <nav>
       <div class="nav-wrapper teal lighten-2">
          <a href="" class="brand-logo"><i class="material-icons">contacts</i>Contactos</a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
              <li><a href="#crearModal">Crear Contacto</a></li>
            </ul>
        </div>
      </nav>
    <div class="container" id="tabla">
        <div class="row">
            <div style="height: 15px"></div>
        </div>
            <div class="row">
                <?php
                  require('partials/tablaLeer.php');
                ?>
            </div> 
    </div> 
<?php
    require('partials/modalCrear.php');
    require('partials/modalActualizar.php');
    require('partials/modalEliminar.php');
    require('partials/modalMensaje.php');
?>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script src="crear.js" type="text/javascript"></script>
    <script src="eliminar.js" type="text/javascript"></script>
    <script src="actualizar.js" type="text/javascript"></script>
    <script src="js/jquery.Rut.js" type="text/javascript"></script>
    <script type="text/javascript">
         $(document).ready(function(){
          Materialize.updateTextFields();
          $('.modal').modal();
          $('select').material_select();
          $('.inputRut').Rut({
              on_error: function(){ 
                alert('RUN no valido!');
                $('.botonContacto').attr('disabled','disabled');
            },
              on_success: function(){ 
                 $('.botonContacto').attr('disabled',false);
            },
              format_on: 'keyup'
            });
          });
         function validarTelefono(e) {
              tecla = (document.all) ? e.keyCode : e.which;
              if (tecla==8) return true;
              patron =/\d/;
              te = String.fromCharCode(tecla);
              return patron.test(te);
          }

      </script>
</body>
</html>